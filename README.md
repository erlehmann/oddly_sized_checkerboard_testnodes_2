# Oddly Sized Checkerboard Test Nodes 2

## Synopsis

The code in this mod generates 25 checkerboard textures in different sizes and registers nodes for them.

The bottom left square is gray, so that nodes placed next to each other are easier to identify.

These nodes can be used to find and debug unintended rendering differences in Minetest.

## Usage

The nodes in this mod can be used to debug rendering issues related to non-16×16 textures sizes and/or texture scaling. This mod is intended to aid creators and users of games, mods or texture packs that contain or generate textures with a size other than 16×16. It can also be used to aid development of filtering and mipmapping code and to prevent rendering regressions when refactoring Minetest code.

To find bugs, place the nodes in this mod in the world and examine if the following assertions hold:

* Nodes with the same tiling (e.g. 2×2) should look alike with different texture filtering settings.

* Any single checkerboard test node should look the same with different versions of Minetest.

* Any single checkerboard test node should look the same when rendered with different GPUs.

If you notice any unintended rendering differences, consider filing issues on Minetest or Mesa:

* <https://github.com/minetest/minetest/issues>
* <https://docs.mesa3d.org/bugs.html>

Rendering differences across GPUs that do not exist in other versions of Minetest are likely Minetest issues. Minetest core developers have claimed in the past that rendering issues are driver issues when they could not reproduce them – if they do, ask Mesa developers about it. Note that using the Mesa software renderer does not necessarily help with determining what the correct output should be.

Rendering differences across GPUs that do not exist in other versions of Mesa are most likely Mesa issues. Note that some Mesa drivers prefer fast rendering over correct rendering and that for some implementation-defined scenarios vendors have long-standing differences in opinion about how results should look.

## History

Minetest does not require regression tests for changes related to rendering. Therefore, rendering changes are not exhaustively tested across many different setups. These nodes were originally created to find and debug regressions related to texture filtering and scaling. They were used to demonstrate a regression with anisotropic filtering on Intel GPUs and to evaluate a patch for gamma-correct mipmap downscaling:

* <https://github.com/minetest/minetest/pull/14006>
* <https://github.com/minetest/minetest/issues/14007>

The code in this mod was offered to be included in Minetest, but the patch was rejected:

* <https://github.com/minetest/minetest/pull/13955>
