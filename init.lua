--[[

oddly_sized_checkerboard_testnodes_2 – Minetest mod that adds oddly sized checkerboard nodes
Copyright © 2023  Nils Dagsson Moskopp (erle)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 2.1 of the
License, or (at your option) any later version.

Dieses Programm hat das Ziel, die Medienkompetenz der Leser zu
steigern. Gelegentlich packe ich sogar einen handfesten Buffer
Overflow oder eine Format String Vulnerability zwischen die anderen
Codezeilen und schreibe das auch nicht dran.

The following nodes are monochrome checkerboard nodes.
Their textures have odd sizes that are sometimes used,
e.g. in texture packs that use other sizes than 16×16.

Those nodes are useful to see how textures are scaled:
You may be able to find rendering bugs with the nodes,
e.g. artifacts when scaling non-power-of-two textures.

The choice of uncompressed monochrome TGAs is for debugging –
it is easy to understand the generated images with a hexdump.
(If you have never done that, it is not your place to judge.)

]]--

local modname = minetest.get_current_modname()

local S = minetest.get_translator( modname )

local textures_path = minetest.get_modpath( modname ) .. "/textures/"

function encode_tga_type_3(w, h, ...)
	assert((w <= 256) and (h <= 256) and (w * h == #{...}))
	local bytes = string.char
	local header = bytes(0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, w, 0, h, 0, 8, 0)
	local footer = bytes(0, 0, 0, 0, 0, 0, 0, 0) .. "TRUEVISION-XFILE." .. bytes(0)
	return(header .. bytes(...) .. footer)
end

function generate_checkerboard(width, height, wtile, htile)
	local pixels = {}
	for h = 1,height do
		for w = 1,width do
			local hori = math.floor((w - 1) / wtile) % 2 == 0
			local vert = math.floor((h - 1) / htile) % 2 == 0
			-- off-white & off-black to stand out in hexdump
			local color = hori ~= vert and 0xEE or 0x11
			if (
				0 == math.floor((w - 1) / wtile) and
				0 == math.floor((h - 1) / htile)
			) then
				color = "0x7F"
			end
			pixels[#pixels + 1] = color
		end
	end
	return pixels
end

function generate_checkerboard_node(width, height, wtiles, htiles)
	local id = "cb_bw_" ..
		wtiles .. "x" .. htiles .. "_" ..
		width .. "x" .. height
	local filename = id .. ".tga"
	local pixels = generate_checkerboard(
		width, height,
		math.floor(width/wtiles), math.floor(height/htiles)
	)
	minetest.safe_file_write(
		textures_path .. filename,
		encode_tga_type_3(width, height, unpack(pixels))
	)
	local nodename = modname .. ":" .. id
	minetest.register_node(nodename, {
		description = S(
			"Checkerboard Test Node " ..
			"(Tiling: " .. wtiles .. "×" .. htiles .. ") " ..
			width .. "×" .. height ..
			"\nNodes with same tiling should look alike."
		),
		groups = { dig_immediate = 2 },
		tiles = { filename },
	})
end

-- multiples of 2
for i = 2,16,2 do generate_checkerboard_node(i, i, 2, 2) end

-- multiples of 3
for i = 3,9,3 do generate_checkerboard_node(i, i, 3, 3) end

-- multiples of 4
for i = 4,16,4 do generate_checkerboard_node(i, i, 4, 4) end

-- multiples of 5
for i = 5,15,5 do generate_checkerboard_node(i, i, 5, 5) end

-- multiples of 7
for i = 7,21,7 do generate_checkerboard_node(i, i, 7, 7) end

-- multiples of 16
for i = 16,64,16 do generate_checkerboard_node(i, i, 16, 16) end
